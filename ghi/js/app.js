// ... Previous code ...

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        const errorAlert = document.querySelector('.error-alert');
        errorAlert.textContent = 'An error occurred while fetching data. Please try again.';
        errorAlert.style.display = 'block';
      } else {
        const data = await response.json();
        const errorAlert = document.querySelector('.error-alert');
        errorAlert.style.display = 'none';

        for (let conference of data.conferences) {
          // ... Rest of the code ...
        }
      }
    } catch (e) {
      const errorAlert = document.querySelector('.error-alert');
      errorAlert.textContent = 'An error occurred. Please try again later.';
      errorAlert.style.display = 'block';
    }
  });
